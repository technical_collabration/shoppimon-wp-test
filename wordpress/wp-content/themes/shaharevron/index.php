<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
        <section id="signup-top">
    <ul id="advantage-list">
        <li>Built for Magento</li>
        <li>Zero Setup</li>
        <li>True User Simulation</li>
        <li>Business uptime monitoring</li>
        <li>Free account</li>
    </ul>
    <div class="content-container signup-form signup-form-vertical">
        <span class="element-container form-action">
            <input value="Get Started Now" type="submit">
        </span>
    </div>
</section><section id="common-features">
    <div class="features-container">
        <div class="feature-block">
            <div class="feature-graphic feature-screenshot">
                <img src="<?php echo bloginfo('template_url') ?>/images/feature-monitoring.png" alt="">
            </div>
            <div class="feature-desc">
                <h3>24/7 Monitoring &amp; Alerts</h3>
                <div>
                    Monitor for downtime and severe issues
                </div>
            </div>
        </div>

        <div class="feature-block">
            <div class="feature-graphic feature-screenshot">
                <img src="<?php echo bloginfo('template_url') ?>/images/feature-sla-reports.png" alt="">
            </div>
            <div class="feature-desc">
                <h3>Weekly SLA Reports</h3>
                <div>
                    Compare your Magento store performance with industry standards. See where
                    you excel and where you fall short.
                </div>
            </div>
        </div>

        <div class="feature-block">
            <div class="feature-graphic feature-screenshot">
                <img src="<?php echo bloginfo('template_url') ?>/images/feature-client-side-issues.png" alt="">
            </div>
            <div class="feature-desc">
                <h3>Client-side Issue Detection</h3>
                <div>
                    Shoppimon uses real browser technology to detect client side
                    issues including CSS and JavaScript errors or missing images.
                </div>
            </div>
        </div>

        <div class="feature-block">
            <div class="feature-graphic feature-screenshot">
                <img src="<?php echo bloginfo('template_url') ?>/images/feature-bottleneck.png" alt="">
            </div>
            <div class="feature-desc">
                <h3>Bottleneck Identification</h3>
                <div>
                    Pinpoint slow performing pages and user scenarios, isolate the root cause and
                    provide a full breakdown for each step.
                </div>
            </div>
        </div>
    </div>
</section>

<section id="shoppimon-edge">
    <h2>The Shoppimon Advantage</h2>

    <div class="feature-block" id="made-for-magento">
        <div class="feature-graphic feature-icon">
            <img src="<?php echo bloginfo('template_url') ?>/images/feature-icon-magento.png" alt="">
        </div>
        <div class="feature-desc">
            <h3>Made for Magento</h3>
            <div>
                Automatically identify and adjust for search extensions, cart and checkout modules
                and additional customizations especially for the Magento platform
            </div>
        </div>
    </div>

    <div class="feature-block" id="zero-setup">
        <div class="feature-graphic feature-icon">
            <img src="<?php echo bloginfo('template_url') ?>/images/feature-icon-zero-setup.png" alt="">
        </div>
        <div class="feature-desc">
            <h3>Zero Setup</h3>
            <div>
                No installation, scripting or code changes! Simply enter the store URL and we'll build
                test scenarios based on your setup, extensions and products.
            </div>
        </div>
    </div>

    <div class="feature-block" id="user-simulation">
        <div class="feature-graphic feature-icon">
            <img src="<?php echo bloginfo('template_url') ?>/images/feature-icon-user-simulation.png" alt="">
        </div>
        <div class="feature-desc">
            <h3>True User Simulation</h3>
            <div>
                Shoppimon doesn't just ping random pages, but actually tests your site the way users
                experience it and allows you to define isolated checks on custom URLs as needed!
            </div>
        </div>
    </div>

    <div class="feature-block" id="full-rendering">
        <div class="feature-graphic feature-icon">
            <img src="<?php echo bloginfo('template_url') ?>/images/feature-icon-full-rendering.png" alt="">
        </div>
        <div class="feature-desc">
            <h3>Full Rendering</h3>
            <div>
                Shoppimon bots actually render visited pages. This means we tell you what your users really
                experience, including resource loading and JavaScript run times.
            </div>
        </div>
    </div>

    <div class="feature-block" id="uptime-monitoring">
        <div class="feature-graphic feature-icon">
            <img src="<?php echo bloginfo('template_url') ?>/images/feature-icon-uptime-monitoring.png" alt="">
        </div>
        <div class="feature-desc">
            <h3>Business Uptime Monitoring</h3>
            <div>
                Shoppimon doesn't only make sure your site is up. We verify that users can complete full
                transactions.
            </div>
        </div>
    </div>

    <div class="feature-block" id="insights">
        <div class="feature-graphic feature-icon">
            <img src="<?php echo bloginfo('template_url') ?>/images/feature-icon-insights.png" alt="">
        </div>
        <div class="feature-desc">
            <h3>Insights on site-wide Issues</h3>
            <div>
                Get suggestions on improving your site's performance, security, search engine optimization
                (SEO) and more.
            </div>
        </div>
    </div>
</section>

<section id="trusted-by">
    <h2>Trusted By</h2>
    <div class="trusted-logo">
        <span><a href="http://www.vaimo.com/" rel="nofollow" target="_blank">
            <img src="<?php echo bloginfo('template_url') ?>/images/logo-vaimo.png" alt="Vaimo">
        </a></span>
        <span><a href="http://www.advancedlogic.eu/" rel="nofollow" target="_blank">
            <img src="<?php echo bloginfo('template_url') ?>/images/logo-advanced-logic.png" alt="Advanced Logic">
        </a></span>
        <span><a href="http://www.blueacorn.com/" rel="nofollow" target="_blank">
            <img src="<?php echo bloginfo('template_url') ?>/images/logo-blue-acorn.gif" alt="Blue Acorn">
        </a></span>
        <span><a href="http://www.balanceinternet.com.au/" rel="nofollow" target="_blank">
            <img src="<?php echo bloginfo('template_url') ?>/images/logo-balance-internet.png" alt="Balance Internet">
        </a></span>
        <span><a href="http://www.younify.nl/" rel="nofollow" target="_blank">
            <img src="<?php echo bloginfo('template_url') ?>/images/logo-younify.png" alt="Younify">
        </a></span>
    </div>
</section>

<section id="signup-bottom" class="signup-form signup-form-horizontal">
    <span class="element-container form-action">
        <input value="Get Started Now" type="submit">
    </span>
</section>

		<?php /*if ( have_posts() ) : ?>

			<?php /* The loop */ ?>
			<?php /*while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php twentythirteen_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; */?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
